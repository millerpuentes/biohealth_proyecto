import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrearMedicoComponent } from './components/crear-medico/crear-medico.component';
import { ListarMedicosComponent } from './components/listar-medicos/listar-medicos.component';

@NgModule({
  declarations: [
    AppComponent,
    CrearMedicoComponent,
    ListarMedicosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
