export class Medico {
    _id?:number;
    primer_nombre: string;
    segundo_nombre: string;
    primer_apellido: string;
    segundo_apellido: string;
    firma: string;

    constructor(primer_nombre: string, segundo_nombre: string, primer_apellido: string, segundo_apellido: string, firma: string){
        this.primer_nombre    = primer_nombre;
        this.segundo_nombre   = segundo_nombre;
        this.primer_apellido  = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.firma            = firma;


    }
}