import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// componentes
import { ListarMedicosComponent } from './components/listar-medicos/listar-medicos.component';
import { CrearMedicoComponent } from './components/crear-medico/crear-medico.component';

const routes: Routes = [
  { path: '',  component:ListarMedicosComponent },
  { path: 'crear-medico', component: CrearMedicoComponent},
  { path: 'editar-medico/:id', component:CrearMedicoComponent},
  { path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
